{
  description = "Maari's system flake";

  nixConfig = {
    extra-substituters = [
      "https://nix-community.cachix.org"
    ];

    extra-trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nixos.url = "github:NixOS/nixpkgs/nixos-unstable";
    nix-darwin.url = "github:LnL7/nix-darwin";
    nix-darwin.inputs.nixpkgs.follows = "nixpkgs";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    neovim-nightly-overlay.url = "github:nix-community/neovim-nightly-overlay";
  };

  outputs = inputs@{ self, nixos, nix-darwin, nixpkgs, home-manager, neovim-nightly-overlay }:
  {
    inherit inputs; # useful to debug and inspect

    # darwin-rebuild build --flake .#water
    darwinConfigurations = import ./darwinConfigurations.nix inputs;

    # sudo nixos-rebuild switch --flake .#water
    nixosConfigurations = import ./nixosConfigurations.nix inputs;
  };
}
