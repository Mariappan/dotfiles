local unused = {
  -- Ctags
  "majutsushi/tagbar",
  "ludovicchabant/vim-gutentags",

  -- HTML. XML tags helper
  --[[ Not needed, provided by treesitter]]
  "AndrewRadev/tagalong.vim",

  --[[ Auto close pairs "'[()]'"]]
  "cohama/lexima.vim",

  --[[ Multiple highlight words ]]
  "vasconcelloslf/vim-interestingwords",

  --[[ REPL ]]
  "metakirby5/codi.vim",
  "max397574/better-escape.nvim",

  -- Toggle comma, semicolon at the end
  "saifulapm/chartoggle.nvim",

  "neovim/nvim-lspconfig",
  "gennaro-tedesco/nvim-jqx",

  "dense-analysis/ale",
}

return {}
