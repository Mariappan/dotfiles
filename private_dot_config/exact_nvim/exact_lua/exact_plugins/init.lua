return {
  -- chezmoi highlight
  { "alker0/chezmoi.vim", event = "VeryLazy" },

  -- FAT FINGER
  { "chip/vim-fat-finger", event = "VeryLazy" },

  -- Buf close helper
  "mhinz/vim-sayonara",
}
