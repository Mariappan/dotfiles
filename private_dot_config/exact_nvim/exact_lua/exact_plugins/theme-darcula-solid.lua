local M = {
  "briones-gabriel/darcula-solid.nvim",
  lazy = true,
  dependencies = {
    "rktjmp/lush.nvim",
  },
}

return M
