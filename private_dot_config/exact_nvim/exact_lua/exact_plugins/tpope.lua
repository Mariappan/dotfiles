return {
  -- Git plugin
  { "tpope/vim-fugitive", event = "VeryLazy" },
  -- Repeat last custom command using '.'
  { "tpope/vim-repeat", event = "VeryLazy" },
  -- Add surround tags like '"[({})]"'
  { "tpope/vim-surround", event = "InsertEnter" },
  -- Edit date efficiently using C-X C-A
  { "tpope/vim-speeddating", event = "InsertEnter" },
  -- Enhance 'ga' showing unicode character
  { "tpope/vim-characterize", event = "VeryLazy" },
}
