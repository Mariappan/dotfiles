-- Decimal, hex, octal, and binary representation
local plugin = {
  "glts/vim-radical",
  lazy = true,
  keys = { "gA" },
  dependencies = {
    "glts/vim-magnum",
    "tpope/vim-repeat",
  },
}

return plugin
