local plugin = {
  "kristijanhusak/vim-dadbod-ui",
  dependencies = {
    "tpope/vim-dadbod",
  },
  cmd = {
    "DBUI",
    "DBUIToggle",
  },
  map = {},
}

return plugin
