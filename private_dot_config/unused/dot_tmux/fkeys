###############################################################################
#    byobu's tmux f-key keybindings
#
#    Copyright (C) 2011-2014 Dustin Kirkland <kirkland@byobu.co>
#
#    Authors: Dustin Kirkland <kirkland@byobu.co>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

# Add F12 to the prefix list
set -g prefix C-a

# Clear the slate
source $HOME/.tmux/fkeys.disable

# force a reload of the config file
unbind r
bind r source-file ~/.tmux.conf\; display-message "Tmux config reloaded"
bind-key -n S-F5 source $HOME/.tmux.conf\; display-message "Tmux config reloaded"
#bind-key -n S-F12 source $HOME/.tmux.conf\; display-message "Tmux config reloaded"

unbind Up
bind Up resize-pane -U
unbind Down
bind Down resize-pane -D
unbind Left
bind Left resize-pane -L
unbind Right
bind Right resize-pane -R

bind-key -n F2 new-window \; rename-window "bash"
bind-key -n F3 copy-mode
bind-key -n F5 command-prompt -p "(rename-window) " "rename-window '%%'"
bind-key -n C-Left previous-window
bind-key -n C-Right next-window
bind-key -n S-Up select-pane -U
bind-key -n S-Down select-pane -D
bind-key -n S-Left select-pane -L
bind-key -n S-Right select-pane -R
bind-key -n C-M-h display-panes \; split-window -h -c "$HOME"
bind-key -n C-M-v display-panes \; split-window -v -c "$HOME"
bind-key -n C-M-l next-layout
bind-key -n M-NPage copy-mode \; send-keys NPage
bind-key -n M-PPage copy-mode \; send-keys PPage
bind-key -n M-IC paste-buffer

bind-key C-a last-window

# Send the same command to all panes/windows
bind e command-prompt -p "Command to all windows:" \
          "run \"tmux list-windows                   -F '##{window_index}'   | xargs -I SESS_WIN \
                 tmux list-panes    -t SESS_WIN      -F 'SESS_WIN.##{pane_index}' | xargs -I SESS_WIN_PANE \
                 tmux send-keys     -t SESS_WIN_PANE '%1' Enter\""

# Send the same command to all panes/windows/sessions
bind E command-prompt -p "Command to all sessions:" \
          "run \"tmux list-sessions                  -F '##{session_name}'        | xargs -I SESS \
                 tmux list-windows  -t SESS          -F 'SESS:##{window_index}'   | xargs -I SESS_WIN \
                 tmux list-panes    -t SESS_WIN      -F 'SESS_WIN.##{pane_index}' | xargs -I SESS_WIN_PANE \
                 tmux send-keys     -t SESS_WIN_PANE '%1' Enter\""

# Byobu's Keybindings
# Documented in: $BYOBU_PREFIX/share/doc/byobu/help.tmux.txt
#bind-key -n F1 new-window -k -n config byobu-config
#bind-key -n S-F1 new-window -k -n help "sh -c '$BYOBU_PAGER $BYOBU_PREFIX/share/doc/byobu/help.tmux.txt'"
#bind-key -n F2 new-window -c "#{pane_current_path}" \; rename-window "-"
#bind-key -n C-M-h display-panes \; split-window -h -c "#{pane_current_path}"
#bind-key -n C-M-v display-panes \; split-window -v -c "#{pane_current_path}"
#bind-key -n C-F2 display-panes \; split-window -h -c "#{pane_current_path}"
#bind-key -n S-F2 display-panes \; split-window -v -c "#{pane_current_path}"
#bind-key -n C-S-F2 new-session
#bind-key -n F3 previous-window
#bind-key -n F4 next-window
#bind-key -n M-Left previous-window
#bind-key -n M-Right next-window
#bind-key -n M-Up switch-client -p
#bind-key -n M-Down switch-client -n
#bind-key -n S-F3 display-panes \; select-pane -t :.-
#bind-key -n S-F4 display-panes \; select-pane -t :.+
#bind-key -n C-F3 display-panes \; swap-pane -s :. -t :.- \; select-pane -t :.-
#bind-key -n C-F4 display-panes \; swap-pane -s :. -t :.+ \; select-pane -t :.+
#bind-key -n C-S-F3 swap-window -t :-1
#bind-key -n C-S-F4 swap-window -t :+1
#
#bind-key -n F5 source $BYOBU_PREFIX/share/byobu/profiles/tmuxrc
#bind-key -n M-F5 run-shell '$BYOBU_PREFIX/lib/byobu/include/toggle-utf8' \; source $BYOBU_PREFIX/share/byobu/profiles/tmuxrc
#bind-key -n S-F5 new-window -k "$BYOBU_PREFIX/lib/byobu/include/cycle-status" \; source $BYOBU_PREFIX/share/byobu/profiles/tmuxrc
#bind-key -n C-F5 send-keys ". $BYOBU_PREFIX/bin/byobu-reconnect-sockets" \; send-keys Enter
#bind-key -n C-S-F5 new-window -d "byobu-select-profile -r"
#bind-key -n F6 detach
#bind-key -n M-F6 run-shell '$BYOBU_PREFIX/lib/byobu/include/tmux-detach-all-but-current-client'
#bind-key -n S-F6 run-shell 'exec touch $BYOBU_RUN_DIR/no-logout' \; detach
#bind-key -n C-F6 kill-pane
#bind-key -n S-F7 capture-pane -S -32768 \; save-buffer "$BYOBU_RUN_DIR/printscreen" \; delete-buffer \; new-window -n "PRINTSCREEN" "view $BYOBU_RUN_DIR/printscreen"
#bind-key -n F8 command-prompt -p "(rename-window) " "rename-window '%%'"
#bind-key -n C-F8 command-prompt -p "(rename-session) " "rename-session '%%'"
#bind-key -n S-F8 next-layout
#bind-key -n M-S-F8 new-window -k "byobu-layout restore; clear; $SHELL"
#bind-key -n C-S-F8 command-prompt -p "Save byobu layout as:" "run-shell \"byobu-layout save '%%'\""
#
#bind-key -n F9 new-window -k -n config byobu-config
#bind-key -n S-F9 command-prompt -p "Send command to all panes:" "run-shell \"$BYOBU_PREFIX/lib/byobu/include/tmux-send-command-to-all-panes '%%'\""
#bind-key -n C-F9 command-prompt -p "Send command to all windows:" "run-shell \"$BYOBU_PREFIX/lib/byobu/include/tmux-send-command-to-all-windows '%%'\""
#bind-key -n M-F9 display-panes \; setw synchronize-panes
#bind-key -n M-F11 break-pane
#bind-key -n C-F11 join-pane -h -s :. -t :-1
bind-key -n S-F11 resize-pane -Z
#bind-key -n S-F12 source $BYOBU_PREFIX/share/byobu/keybindings/fkeys.tmux.disable \; display-message "Byobu F-keys: DISABLED"
#bind-key -n C-S-F12 new-window $BYOBU_PREFIX/lib/byobu/include/mondrian
#bind-key -n M-F12 source $BYOBU_PREFIX/share/byobu/keybindings/mouse.tmux.enable

#bind-key -n C-a new-window -n "ctrl-a" "byobu-ctrl-a"

