status is-interactive || exit 0

# Lightway alias - START
abbr -a lwc "cargo fmt && cargo clippy --fix --allow-dirty --allow-staged --all-features --all-targets -- -D warnings && cargo nextest run"

set -U __user_lws "cargo build --bin lightway-server && sudo -E ip netns exec lightway-server target/debug/lightway-server --config-file=tests/ignore_custom_server_conf.yaml"
set -U __user_lwc "cargo build --bin lightway-client && sudo -E ip netns exec lightway-client target/debug/lightway-client --config-file=tests/ignore_custom_client_conf.yaml"

abbr -a -- lwst $__user_lws
abbr -a -- lwct $__user_lwc

abbr -a -- lwsu $__user_lws --mode udp
abbr -a -- lwcu $__user_lwc --mode udp

abbr -a -- bashc "sudo ip netns exec lightway-client bash"
abbr -a -- pingc "sudo ip netns exec lightway-client ping google.com -s 100 -c 2"

abbr -a -- iperfs "sudo ip netns exec lightway-remote iperf3 -s -B 8.8.8.8"

set -U __user_iperfc "sudo ip netns exec lightway-client iperf3 -c 8.8.8.8 -V -b 2G -l 1200"
abbr -a -- iperfct $__user_iperfc
abbr -a -- iperfcu $__user_iperfc -u

# Lightway alias - END

