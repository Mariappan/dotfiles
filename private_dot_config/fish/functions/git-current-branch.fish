function git-current-branch
    set -f ref "$(command git symbolic-ref HEAD 2> /dev/null)"
    if test -z $ref
        return 1
    else
        echo (string replace refs/heads/ '' $ref)
        return 0
    end
end
