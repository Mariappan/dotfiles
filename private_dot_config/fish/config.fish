if status is-interactive
    # Ctrl L - Clear the screen, but dont clear the scrollback
    bind \cl 'for i in (seq 1 $LINES); echo; end; clear; commandline -f repaint'
    bind \cw backward-kill-word
end
