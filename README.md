# Maari's Dotfiles

Dotfiles managed using [chezmoi](https://www.chezmoi.io/)


## MacOS Setup:

- Install brew (https://brew.sh)
```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

- Install chezmoi
```bash
/opt/homebrew/bin/brew install chezmoi
```

- Init chezmoi and apply config
```bash
/opt/homebrew/bin/chezmoi init --apply gitlab.com/mariappan
```

- Restart shell and install dependencies
```bash
zsh
brew install git-delta
brew install direnv
brew install bat
brew install neovim
```

## Updating prezto:

```bash
git submodule update --remote --merge
git submodule update --init --recursive
```
